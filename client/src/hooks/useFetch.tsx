import { useState, useEffect } from 'react'

const useFetch = (endpoint: string) => {
    const baseUrl = 'https://opentdb.com/'

    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        fetch(`${baseUrl}${endpoint}`)
            .then(d => d.json())
            .then(d => {
                // console.log(d)
                setData(d)
                setLoading(false)
            })
    }, [endpoint])

    return { data, loading }
}

export default useFetch