import { useDispatch } from 'react-redux';
import { setDifficulty } from '../features/gameplaySettingsSlice';

import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';

import { difficulties } from '../utils/utils';

import { useSelector } from 'react-redux';
const DifficultySelectField = () => {
    const dispatch = useDispatch()
    const difficulty = useSelector(state => state.gameplaySettingsSlice.difficulty)


    const handleChange = (e) => {
        dispatch(setDifficulty(e.target.value))
    }

    return (
        <FormControl
            fullWidth
            size='small'
            sx={{
                border: '1px solid black',
                borderRadius: '5px',
                bgcolor: "#1565C0",
            }}
        >
            <InputLabel sx={{ color: "white", }}>Choose Difficulty</InputLabel>
            <Select value={difficulty} onChange={e => handleChange(e)}>
                {
                    difficulties.map((item, i) => {
                        return <MenuItem
                            key={i + 1}
                            value={item}
                        >
                            {item}
                        </MenuItem>

                    })
                }

            </Select>
        </FormControl >
    )
}

export default DifficultySelectField