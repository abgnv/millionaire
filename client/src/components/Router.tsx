import { Routes, Route } from 'react-router-dom'

import LandingScreen from '../views/LandingScreen/LandingScreen'
import QuestionScreen from '../views/QuestionScreen'
import EndScreen from '../views/EndScreen'
EndScreen

const Router = () => {
    return (
        <Routes>
            <Route path="/" element={<LandingScreen />}></Route>
            <Route path="/q" element={<QuestionScreen />}></Route>
            <Route path="/end" element={<EndScreen />}></Route>
        </Routes>
    )
}

export default Router