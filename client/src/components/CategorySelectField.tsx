import { useDispatch } from 'react-redux';
import { setCategory, } from '../features/gameplaySettingsSlice';

import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';

import useFetch from '../hooks/useFetch';

import { useSelector } from 'react-redux';

const CategorySelectField = () => {
    const { data } = useFetch('api_category.php')
    const category = useSelector(state => state.gameplaySettingsSlice.category)

    const dispatch = useDispatch()

    const handleChange = (e) => {
        dispatch(setCategory(e.target.value))
    }

    // const categoriesWithEnoughQs1 = [];
    // const [categoriesWithEnoughQs, setCategoriesWithEnoughQs] = useState([])
    // console.log(categoriesWithEnoughQs);

    // //тва тука с презаписването на масива??
    // useEffect(() => {
    //     data.trivia_categories?.map(c => {
    //         const data2 = fetch(`https://opentdb.com/api_count.php?category=${(c.id).toString()}`)
    //             .then(res => res.json())
    //             .then(res => {
    //                 if (
    //                     res.category_question_count.total_easy_question_count >= 15
    //                     && res.category_question_count.total_medium_question_count >= 15
    //                     && res.category_question_count.total_hard_question_count >= 15
    //                 ) {
    //                     categoriesWithEnoughQs1.push(c.id)
    //                 }
    //                 setCategoriesWithEnoughQs(categoriesWithEnoughQs1);
    //             })
    //     })
    // }, [data])

    // console.log('cat', categoriesWithEnoughQs);
    // const categoriesToDisplay = data.trivia_categories?.filter(c => categoriesWithEnoughQs.includes(c.id))


    // console.log(categoriesToDisplay);

    // const test = getCategory((state) => state.gameplaySettingsSlice.)



    return (
        <FormControl
            fullWidth
            size='small'
            sx={{
                borderRadius: '5px',
                bgcolor: "#1565C0",
            }}
        >
            <InputLabel sx={{ color: "white" }}>Choose Category</InputLabel>
            <Select value={category} onChange={e => handleChange(e)} >
                {
                    data.trivia_categories?.map((item) => {
                        return <MenuItem
                            key={item.id}
                            value={item.id}
                            defaultValue="9"
                        >
                            {item.name}
                        </MenuItem>
                    })
                }
            </Select>
        </FormControl >
    )
}


export default CategorySelectField