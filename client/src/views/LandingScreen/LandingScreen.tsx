import { useState } from 'react'

import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

import { Button, Box } from '@mui/material';
import '../LandingScreen.css'

import CategorySelectField from '../../components/CategorySelectField';
import DifficultySelectField from '../../components/DifficultySelectField';

import { useNavigate } from 'react-router-dom'

const LandingScreen = () => {
    const [isStarted, setIsStarted] = useState(false)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const category: string = useSelector(state => state.gameplaySettingsSlice.category)

    const difficulty: string = useSelector(state => state.gameplaySettingsSlice.difficulty)

    const handleStartClick = () => {
        setIsStarted(true)
    }

    const handleContinueClick = () => {
        navigate('/q')
    }


    return (
        <Box
            className='background-container'
            sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh',
            }}>

            {!isStarted ?
                <Button
                    variant="contained"
                    color="primary"
                    sx={{ width: '50vh', height: '8vh', m: '5px', fontSize: '18px' }}
                    onClick={handleStartClick}
                >
                    Start Game
                </Button>
                :
                <>
                    <Box width={'400px'}>
                        <Box m={'5px'}>
                            <CategorySelectField />
                        </Box>
                        <Box m={'5px'}>
                            <DifficultySelectField />
                        </Box>

                        {category && difficulty &&
                            <Box >
                                <Button variant='contained' sx={{ width: '400px' }} onClick={handleContinueClick}>CONTINUE</Button>
                            </Box>
                        }
                    </Box>
                </>
            }
        </Box >
    );
};

export default LandingScreen;