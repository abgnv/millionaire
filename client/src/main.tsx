import ReactDOM from 'react-dom/client'

import { Provider } from 'react-redux'
import { store } from './store/store.ts'

import { BrowserRouter } from 'react-router-dom'

import CssBaseline from '@mui/material/CssBaseline/CssBaseline'

import App from './App.tsx'
import './index.css'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <>
    <BrowserRouter>
      <Provider store={store}>
        <CssBaseline />
        <App />
      </Provider>
    </BrowserRouter>
  </>
)
