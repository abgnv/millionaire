const random = (q) => {
    const res = [...q.incorrect_answers, q.correct_answer]

    const shuffledArray = []
    const usedIndexes = []
    let i = 0

    while (i < res.length) {
        const randomNumber = Math.floor(Math.random() * res.length)

        if (!usedIndexes.includes(randomNumber)) {
            shuffledArray.push(res[randomNumber])
            usedIndexes.push(randomNumber)

            i++
        }
    }

    return shuffledArray
}

export default random