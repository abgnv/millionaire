import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    category: '',
    difficulty: '',
};


const gameplaySettingsSlice = createSlice({
    name: "gameplay",
    initialState,
    reducers: {
        setCategory: (state, action) => {
            state.category = action.payload;
        },
        setDifficulty: (state, action) => {
            state.difficulty = action.payload;
        },
    },
});


export const { setCategory, setDifficulty, getCategory } = gameplaySettingsSlice.actions;
export default gameplaySettingsSlice.reducer;