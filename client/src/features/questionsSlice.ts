import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    questionCount: 0,
    questions: [],
    rightAnswer: '',
    answer: '',
    clickedButton: {
        id: '',
        marked: '',
        true: '',
        false: '',
        timestamp: ''
    }
};


const questionsSlice = createSlice({
    name: "questions",
    initialState,
    reducers: {
        incrQuestionCount: (state) => {
            state.questionCount += 1;
        },
        setQuestions: (state, action) => {
            state.questions = action.payload;
        },
        setAnswer: (state, action) => {
            state.answer = action.payload;
        },
        setRightAnswer: (state, action) => {
            state.rightAnswer = action.payload;
        },
        setClickedButton: (state, action) => {
            state.clickedButton = action.payload;
        },
    },
});


export const { incrQuestionCount, setQuestions, setAnswer, setRightAnswer, setClickedButton } = questionsSlice.actions;
export default questionsSlice.reducer;