import { configureStore } from "@reduxjs/toolkit";

import gameplaySettingsSlice from "../features/gameplaySettingsSlice";
import questionsSlice from "../features/questionsSlice";

export const store = configureStore({
    reducer: {
        gameplaySettingsSlice,
        questionsSlice,
    },
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;